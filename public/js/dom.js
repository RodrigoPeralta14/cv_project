export default class Dom {


    constructor() {

        this._observer = null;
        this.init();

    }

    init() {

        const options = {
            rootMargin: '0px',
            threshold:  0.1,
        };

        if(IntersectionObserver == null) {
            
            for (const target of [...document.querySelectorAll('.defered')]) {

                target.classList.add('shouldBeVisible');
    
            }

            
            
        } else {
            
            this._observer = new IntersectionObserver((entryList) => {

                for (const entry of entryList) {
    
                    if (!(entry.isVisible) && entry.isIntersecting) {
    
                        entry.target.classList.add('shouldBeVisible');
    
                    }
    
                }
    
            }, options);
    
            this.observeAll();
            
            
        }

        this.prepareDarkmode();
        

    }

    observeAll() {

        for (const target of [...document.querySelectorAll('.defered')]) {

            this._observer.observe(target);

        }

    }

    prepareDarkmode() {

        document.querySelector('#darkmode')
                .addEventListener('click', () => {

                    this.toggleDarkmode();

                });

        document.querySelector('#darkmode')
                .addEventListener('touchstart', () => {

                    this.toggleDarkmode();

                });

    }

    toggleDarkmode() {

        document.documentElement.classList.toggle('darkMode');
        document.querySelector('#darkModeIcon').classList.toggle('mdi-lightbulb-outline');
        document.querySelector('#darkModeIcon').classList.toggle('mdi-lightbulb-on-outline');

    }

}
