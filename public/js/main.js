(() => {

    document.addEventListener('DOMContentLoaded', () => {
        Main.run();
    })

})();


import Dom from "./dom.js";

export default class Main {


    constructor() {

        this._dom = null;
        this.init();

    }

    static run() {

        new Main();

    }


    init() {

        this._dom = new Dom();

    }


}